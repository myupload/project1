const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors('*'))

app.listen(3000,'0.0.0.0',()=>{
    console.log("Server started on port no. 3000");
})